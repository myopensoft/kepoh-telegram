<?php

return [
    'telegram' => [
        'enable' => env('KEPOH_TELEGRAM_ENABLE', false),
        'bot_token' => env('KEPOH_TELEGRAM_BOT_TOKEN'),
        'chat_id' => env('KEPOH_TELEGRAM_CHAT_ID'),
        'dont_report' => [
            \Illuminate\Auth\AuthenticationException::class,
            \Illuminate\Auth\Access\AuthorizationException::class,
            \Illuminate\Session\TokenMismatchException::class,
            \Illuminate\Validation\ValidationException::class,
        ]
    ]
];
