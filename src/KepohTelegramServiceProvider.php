<?php

namespace MyOpensoft\KepohTelegram;

use Illuminate\Support\ServiceProvider;

class KepohTelegramServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/config/myopensoft-kepoh.php', 'myopensoft-kepoh'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/myopensoft-kepoh.php' => config_path('myopensoft-kepoh.php'),
        ]);
    }
}
