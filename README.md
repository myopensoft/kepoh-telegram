# Kepoh Telegram

For Laravel 5.7+, 6.0+

Add setting to `composer.json`
```
"repositories": [
    {
        "type": "vcs",
        "url": "https://gitlab.com/myopensoft/kepoh-telegram"
    }
],

...

"myopensoft/kepoh-telegram": "dev-master",
```

Add setting to `.env`
```
KEPOH_TELEGRAM_ENABLE=true
KEPOH_TELEGRAM_BOT_TOKEN=your_bot_token_here
KEPOH_TELEGRAM_CHAT_ID=@your_channel_chat_id
```


Add code to `app\Exception\Handler.php`
```PHP
use Request;

...

public function report(Exception $exception)
{
    KepohTelegram::report($exception);

    ...
}
```

Publish config `php artisan vendor:publish`
